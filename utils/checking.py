"""Методы для проверки наших запросов"""
import json

from requests import Response


class Checking:

    """Метод для проверки статус кода"""
    @staticmethod
    def check_status_code(response: Response, status_code):
        sc_res = response.status_code
        assert sc_res == status_code
        match sc_res:
            case 100 | 102:
                print(f'Статус в обработке, статус код = {sc_res}')
            case 200 | 202:
                print(f'Успешно, статус код = {sc_res}')
            case 301 | 302:
                print(f'Статус перенаправления, статус код = {sc_res}')
            case 401 | 404:
                print(f'Статус ошибки, статус код = {sc_res}')
            case 500 | 503:
                print(f'Статус ошибки на сервере, статус код = {sc_res}')
            case _:
                print(f'Статус код = {sc_res}')


    """Метод для проверки наличия обязательных полей в ответе запроса"""
    @staticmethod
    def check_json_token(response: Response, expected_value):
        token = json.loads(response.text)
        assert list(token) == expected_value
        print(f'Все обязательные поля присутствуют')


    """Метод для проверки значений обязательных полей в ответе запроса"""
    @staticmethod
    def check_json_value(response: Response, field_name, expected_value):
        check = response.json()
        check_info = check.get(field_name)
        assert check_info == expected_value
        print(f'{field_name} верен!')

    """Метод для проверки значений обязательных слов в поле ответа запроса"""
    @staticmethod
    def check_json_word_in_value(response: Response, field_name, expected_word_in_value):
        check = response.json()
        check_info = check.get(field_name)
        if expected_word_in_value in check_info:
            assert expected_word_in_value in check_info
            print(f'В поле {field_name} обязательные слова присутствуют!')
        else:
            print(f'В поле {field_name} обязательные слова отсутствуют!')
